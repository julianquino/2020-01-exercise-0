package ar.uba.fi.tdd.exercise;

class ArticleConjured implements Article {

	protected Item item;
	public ArticleConjured(Item _item){
		this.item = _item;
	}

	public void articleUpdate(){
		int qualitySubtractionValue = QUALITYPOINTS*DOUBLEQUALITYFACTOR;
		if(this.item.sellIn <= LASTSELLDATE)
			qualitySubtractionValue = QUALITYPOINTS*DOUBLEQUALITYFACTOR;
		if (this.item.quality > MINQUALITY)
			this.item.quality -= qualitySubtractionValue;
	}

	public int getQuality(){
		return this.item.quality;
	}

	public String getName(){
		return this.item.Name;
	}
}