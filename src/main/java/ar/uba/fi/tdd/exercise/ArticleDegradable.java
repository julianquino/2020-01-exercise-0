package ar.uba.fi.tdd.exercise;

class ArticleDegradable implements Article {

	protected Item item;
	public ArticleDegradable(Item _item){
			this.item = _item;
	}

	public void articleUpdate(){
		int qualitySubtractionValue = QUALITYPOINTS;
		if(this.item.sellIn <= LASTSELLDATE)
			qualitySubtractionValue = QUALITYPOINTS*DOUBLEQUALITYFACTOR;
		if (this.item.quality > MINQUALITY)
			this.item.quality = this.item.quality - qualitySubtractionValue;
	}

	public int getQuality(){
			return this.item.quality;
	}

	public String getName(){
		return this.item.Name;
	}
}