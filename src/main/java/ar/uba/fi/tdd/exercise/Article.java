
package ar.uba.fi.tdd.exercise;

interface Article {

	public static final int MINQUALITY = 0;
	public static final int MAXQUALITY = 50;
	public static final int LASTSELLDATE = 0;
	public static final int QUALITYPOINTS = 1;
	public static final int QUALITYNULL = 0;
	public static final int DOUBLEQUALITYFACTOR = 2;
	public static final int TRIPLEQUALITYFACTOR = 3;

	public abstract void articleUpdate();
	public abstract int getQuality();
	public abstract String getName();
}
