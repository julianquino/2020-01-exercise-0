package ar.uba.fi.tdd.exercise;

class GildedRose {
	Article[] items;

	public GildedRose(Article[] _items) {
		this.items = _items;
	}

	// update the quality of the emements
	public void updateQuality() {
			// for each item
		for (int i = 0; i < items.length; i++) {
			items[i].articleUpdate();
		}
	}
}
