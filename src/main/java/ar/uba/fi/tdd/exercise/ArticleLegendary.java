package ar.uba.fi.tdd.exercise;

class ArticleLegendary implements Article {
	protected Item item;
	public ArticleLegendary(Item _item){
		this.item = _item;
	}

	public void articleUpdate(){}

	public int getQuality(){
		return this.item.quality;
	}

	public String getName(){
		return this.item.Name;
	}
}