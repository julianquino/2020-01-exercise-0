package ar.uba.fi.tdd.exercise;

class ArticleTicket implements Article {
	protected Item item;
	public static final int EXPIRATIONINSIXDAYS = 5;
	public static final int EXPIRATIONINTENDAYS = 10;

	public void ArticleTicket(Item _item){
		this.item = _item;
	}

	public void articleUpdate(){
		if (this.item.sellIn <= LASTSELLDATE)
			this.item.quality = QUALITYNULL;
		else
			this.updateQuality();
	}

	public int getQuality(){
		return this.item.quality;
	}

	private void updateQuality(){
		if (this.item.quality <= MAXQUALITY) {
			if (this.item.sellIn <= EXPIRATIONINSIXDAYS) {
				this.item.quality+=QUALITYPOINTS*TRIPLEQUALITYFACTOR;
			}else if (this.item.sellIn <= EXPIRATIONINTENDAYS){
				this.item.quality+=QUALITYPOINTS*DOUBLEQUALITYFACTOR;
			}else
				this.item.quality+=QUALITYPOINTS;
		}
	}

	public String getName(){
		return this.item.Name;
	}
}