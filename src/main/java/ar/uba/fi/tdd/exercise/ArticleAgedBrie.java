package ar.uba.fi.tdd.exercise;

class ArticleAgedBrie implements Article {
	protected Item item;

	public ArticleAgedBrie(Item _item){
		this.item = _item;
	}  

	public void articleUpdate(){
		if (this.item.quality <= MAXQUALITY) {
			this.item.quality+=QUALITYPOINTS;
		}
	}

	public int getQuality(){
		return this.item.quality;
	}

	public String getName(){
		return this.item.Name;
	}
}