package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class ArticleTest {

	@Test
	public void legendaryItemNotLowQuality() {
		Article legendary = new ArticleLegendary(new Item("fixme", 0, 80));
		legendary.articleUpdate(); 
		assertThat(80).isEqualTo(legendary.getQuality());
	}

	@Test
	public void legendaryItemLowQuality() {
		Article legendary = new ArticleDegradable(new Item("fixme", 10, 10));
		legendary.articleUpdate();
		assertThat(9).isEqualTo(legendary.getQuality());
	}
}
