package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GildedRoseTest {

	@Test
	public void foo() {
		Article[] items = new Article[] { new ArticleLegendary(new Item("fixme", 10, 80)) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat("fixme").isEqualTo(app.items[0].getName());
	}

	@Test
	public void itemWithExpiredSellDegradesTwice() {
		Article[] items = new Article[] { new ArticleDegradable(new Item("fixme", 0, 10)) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(8).isEqualTo(app.items[0].getQuality());
	}
}
